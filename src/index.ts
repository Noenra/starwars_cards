const btn:HTMLElement = document.getElementById("btn");
let image:HTMLElement = document.getElementById('img');
let cardName: HTMLElement = document.getElementById('name')

function cards(){
    let numberAlea: number = 0;
    let type: Array<string> = ['people', 'planets', 'species', 'starships', 'vehicles'];
    let typeRandom: number = Math.floor(Math.random() * type.length);
    if(typeRandom === 0){
        numberAlea = Math.floor(Math.random() * 82);
    }
    else if(typeRandom === 1){
        numberAlea = Math.floor(Math.random() * 60);
    }
    else if(typeRandom === 2){
        numberAlea = Math.floor(Math.random() * 37);
    }
    else if(typeRandom === 3){
        numberAlea = Math.floor(Math.random() * 36);
    }
    else{
        numberAlea = Math.floor(Math.random() * 39);
    }

    fetch(`https://swapi.dev/api/${type[typeRandom]}/${numberAlea}`)
    .then(resp => resp.json())
    .then(data => {
        if(data.name !== undefined){
            //@ts-ignore
            image.src = `assets/${type[typeRandom]}/${numberAlea}.jpg`
            //@ts-ignore
            image.alt = `${data.name}`
            cardName.innerHTML = `${data.name}`
    
    
            console.log(data);
            console.log(type[typeRandom]);
            console.log(data.name);
        }else {
            //@ts-ignore
            image.src = `assets/you_lose.png`
            //@ts-ignore
            image.alt = `Perdu`
            cardName.innerHTML = `Relancer tu dois, mais attendre 2 heures.`
        }
    })
    .catch(error =>{
        console.log(error);
    });
}

    function time(){
        const now = new Date().getTime();
        const diff = parseInt(localStorage.getItem('2h')) - now;

        const hours = Math.floor((diff % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60))
        const minutes = Math.floor((diff % (1000 * 60 * 60)) / (1000 * 60))
        const seconds = Math.floor((diff % (1000 * 60)) / 1000)
        
        if(diff < 0 || diff !== undefined ){
            btn.innerText = `${hours}h ${minutes}m ${seconds}s`
        }else{
            clearInterval(timer)
            btn.innerText = `Cliquer tu dois`
            btn.addEventListener('click', () => {
                const plus2h = new Date(now + 7200000).getTime();

                localStorage.setItem('2h', JSON.stringify(plus2h));
                cards();
                timer
            });
        }
    }
let timer = setInterval(() => {
    time();
}, 1000);


function distanceBase(distanceBase: any) {
    throw new Error("Function not implemented.");
}
// let counter = 10;
// let setTimer: number = 1000

